import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { PanelComponent } from './components/panel/panel.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' }, // Redirige a la ruta '/home' por defecto
  // Otras rutas existentes...
  { path: 'login', component: LoginComponent },
  // Otras rutas existentes...
  { path: 'home', component: SideMenuComponent },
  { path: 'panel', component: PanelComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {


}

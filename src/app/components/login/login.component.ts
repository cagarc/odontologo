import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form!: FormGroup;

  constructor(private fb: FormBuilder,
              private router: Router,) { }

  ngOnInit() {
    this.form = this.fb.group({
      username: [''],
      password: ['']
    });
  }


  login() {
    const { username, password } = this.form.value;
    this.router.navigate(['panel']);

  }
}

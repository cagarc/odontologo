import { Component, Input, OnInit, TemplateRef } from '@angular/core';


@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.scss']
})
export class SideMenuComponent implements OnInit {

  menuOpen: boolean = false;
  @Input()
  contentTemplate!: TemplateRef<any>;
  toggleMenu(): void {
    this.menuOpen = !this.menuOpen;
  }
  ngOnInit(): void {

  }

}

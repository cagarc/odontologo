import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';


@NgModule({
  declarations: [],
  imports: [
    // Agrega aquí los demás módulos de Angular Material que necesites
    CommonModule,
    MatListModule,
    MatIconModule,
  ],
  exports: [
    MatListModule,
    MatIconModule,
    MatMenuModule,
    // Agrega aquí los demás módulos de Angular Material que necesites
  ]
})
export class MaterialModule { }
